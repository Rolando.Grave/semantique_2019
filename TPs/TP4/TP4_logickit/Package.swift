// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TP4_logickit",
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/kyouko-taiga/LogicKit", .branch("master")),    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "TP4_logickit",
            dependencies: ["LogicKit"]),
        .testTarget(
            name: "TP4_logickitTests",
            dependencies: ["TP4_logickit"]),
    ]
)
