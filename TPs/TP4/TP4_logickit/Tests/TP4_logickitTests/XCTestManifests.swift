import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TP4_logickitTests.allTests),
    ]
}
#endif
