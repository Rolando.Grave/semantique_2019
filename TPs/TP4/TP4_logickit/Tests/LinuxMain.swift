import XCTest

import TP4_logickitTests

var tests = [XCTestCaseEntry]()
tests += TP4_logickitTests.allTests()
XCTMain(tests)
