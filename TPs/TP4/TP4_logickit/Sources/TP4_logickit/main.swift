import LogicKit
import LogicKitBuiltins

// Initial Term
// You can add other terms if needed

//let x: Term = .var("x")


// KnowledgeBase is a collection which contains all the things we know
var kb: KnowledgeBase = [

  // Complete the facts and rules in this knowledge base
  // HERE

]

// We merge your knowledge base with builtins types in LogicKit
// Also, you can use all operation on Nat and List
kb = kb + KnowledgeBase(knowledge: (List.axioms + Nat.axioms))


// Write your examples to test your rules below !
