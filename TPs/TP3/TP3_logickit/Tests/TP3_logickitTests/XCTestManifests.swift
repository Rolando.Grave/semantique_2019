import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TP3_logickitTests.allTests),
    ]
}
#endif
