import XCTest

import TP3_logickitTests

var tests = [XCTestCaseEntry]()
tests += TP3_logickitTests.allTests()
XCTMain(tests)
